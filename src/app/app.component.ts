import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Notification } from './Model/Notification';
import { NotificationService } from './service/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private notificationService: NotificationService) {}

  onFormSubmitted(form: NgForm) {

    const notification: Notification = {
      message: form.value.notificationMsg,
      type: form.value.type,
      status: false,
    };

    this.notificationService.AddNewNotificationMessage(notification);
  }
}
