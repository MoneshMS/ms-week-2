import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { Notification } from '../Model/Notification';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  NotificationMessages: Notification[] = [];
  notifiaction: Subject<Notification[]> = new Subject<Notification[]>();

  notificationColors = {
    success: ['success'],
    error: ['error'],
    warn: ['warn'],
    info: ['info'],
  };

  constructor(private snackbar: MatSnackBar) {}

  openMatSnackBar(id: number) {
    const notification = this.NotificationMessages[id];
    notification.status = true;
    this.notifiaction.next(this.NotificationMessages);
    this.snackbar.open(notification.message, 'x', {
      duration: 2000,
      panelClass: this.notificationColors[notification.type],
      verticalPosition: 'top',
    });
  }

  getNotificationMessages() {
    return this.notifiaction.asObservable();
  }

  AddNewNotificationMessage(notification: Notification) {
    this.NotificationMessages.push(notification);
    this.notifiaction.next(this.NotificationMessages);
  }
}
