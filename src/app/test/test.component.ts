import { Component, Input, OnInit } from '@angular/core';
import { Notification } from '../Model/Notification';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  notificationArray: Notification[] = [];

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationService.getNotificationMessages().subscribe((res) => {
      this.notificationArray = res;
    });
  }

  openNotification(i: number) {
    this.notificationService.openMatSnackBar(i);
  }
}
